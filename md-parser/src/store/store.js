import Vuex from 'vuex'
import Vue from 'vue'
Vue.use(Vuex)


export const store = new Vuex.Store({
  state: {
    article: {
      meta: {
        author: '',
        date_created: '',
        date_modfied: '',
        title: '',
      },
      content: "",
    }
  },
  getters: {
    getContent(state) {
      return state.article.content
    }
  },
  mutations: {
    updateText (state, newText) {
      state.article.content = newText
    },
    updateMeta (state, newMeta) {
      state.article.meta = newMeta
    },
    today (state, date) {
      let dateObj = new Date()
      let today = dateObj.getDate() + '/' + dateObj.getMonth() + '/' + dateObj.getFullYear()
      if (date === 1) {
        state.article.meta.date_created = today
      }
      else if (date === 2) {
        state.article.meta.date_modified = today
      }
    },
    resetAll (state) {
      state.article.meta = {}
      state.article.content = ""
    }
  },
  actions: {
    updateText(context, newText){
      context.commit('updateText', newText)
    }
  }
})

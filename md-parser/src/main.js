import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import EditPage from './components/EditPage.vue'
import UserInputPage from './components/UserInputPage.vue'
import PublishedPage from './components/PublishedPage.vue'
import Vuex from 'vuex'
import BootstrapVue from 'bootstrap-vue'
import VueResource from 'vue-resource'
import { store } from './store/store'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueMaterial from 'vue-material'

Vue.use(VueRouter)
Vue.use(BootstrapVue)
Vue.use(VueResource)
Vue.use(Vuex)

Vue.use(VueMaterial)

Vue.config.productionTip = false

const routes = [
  { path: '/edit', component : EditPage},
  { path: '/write', component: UserInputPage},
  { path: '/published', component: PublishedPage }
]

const router = new VueRouter({
  routes
})

new Vue({
  router,
  render: h => h(App),
  store: store,
}).$mount('#app')


# md-parser
**This is a work in progress** 

VueJS-based frontend to plug on a Pelican static site. 

Gives a nice UI to edit and publish articles.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

or : 
```
vue serve src/
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
